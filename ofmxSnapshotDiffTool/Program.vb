Imports System
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text

Module Program

    Public master As New Xml.XmlDocument
    Public slave As New Xml.XmlDocument

    Public directory As String = ""

    Public filter As String

    Public CLASS_region As String = ""

    Public CLASS_bounds As String = ""


    Dim version = "1.0"
    Sub writeHeader()
        Console.BackgroundColor = ConsoleColor.Gray
        Console.ForegroundColor = ConsoleColor.Black

        Console.WriteLine("(c) open flightmaps association - Switzerland")
        Console.WriteLine("OFMX SNAPSHOT DIFF TOOL" & "    VERSION: " & version & " ")
        Console.ResetColor()
    End Sub


    Dim coreFeatures As New List(Of coreFeatureStruct)

    Sub Main(args As String())
        writeHeader()



        For i As Short = 0 To args.Count - 1

            Try
                If args(i) = "-d" Then
                    directory = args(i + 1)
                    Console.WriteLine("OFMX directory loaded: " & args(i + 1))
                End If
            Catch ex As Exception
                Console.WriteLine("ERR: cant load directory ofmx file! " & ex.Message)
            End Try


            Try
                If args(i) = "-b" Then
                    CLASS_bounds = args(i + 1)
                    Console.WriteLine("bounds loaded: " & args(i + 1))
                End If
            Catch ex As Exception
                Console.WriteLine("ERR: cant load directory ofmx file! " & ex.Message)
            End Try

            Try
                If args(i) = "-m" Then
                    master.Load(args(i + 1))
                    Console.WriteLine("OFMX master file loaded: " & args(i + 1))
                End If


            Catch ex As Exception
                Console.WriteLine("ERR: cant load master ofmx file! " & ex.Message)
            End Try

            Try
                If args(i) = "-r" Then
                    CLASS_region = args(i + 1)
                    Console.WriteLine("region: " & args(i + 1))
                End If


            Catch ex As Exception
                Console.WriteLine("ERR: cant load master ofmx file! " & ex.Message)
            End Try

            Try
                If args(i) = "-s" Then
                    slave.Load(args(i + 1))
                    Console.WriteLine("OFMX slave file loaded: " & args(i + 1))
                End If

            Catch ex As Exception
                Console.WriteLine("ERR: cant load slave ofmx file! " & ex.Message)
            End Try


            Try
                If args(i) = "-f" Then
                    Dim inFi = args(i + 1).ToLower
                    Console.WriteLine("filter loaded: " & args(i + 1))

                    If inFi.Contains("airports") Then
                        filter &= "Ahp,Rwy,Rdd,Rdn,Twy,Apn,Tly,Msc,Ahu,,Fto,Tlo,Tls,Rls,Fdn,Fdd,Fdo,Fda,Fls,Tla,Aha"
                    End If
                    If inFi.Contains("airspaces") Then
                        filter &= "Ase,Abd,Adg,Sae,"
                    End If
                    If inFi.Contains("units") Then
                        filter &= "Fqy,Ser,Uni,"
                    End If
                    If inFi.Contains("procedures") Then
                        filter &= "Prc,"
                    End If
                    If inFi.Contains("organizations") Then
                        filter &= "Org,"
                    End If
                    If inFi.Contains("obstacles") Then
                        filter &= "Obs,Ogr,"
                    End If
                    If inFi.Contains("platepackages") Then
                        filter &= "Ppa,Plp,"
                    End If
                    If inFi.Contains("navaids") Then
                        filter &= "Dpn,Vor,Dme,Ndb"
                    End If

                End If

            Catch ex As Exception
                Console.WriteLine("ERR: cant read filter -f! " & ex.Message)
            End Try
        Next


        If filter = "" Then filter = "Ase,Abd,Adg,Ahp,Rwy,Rdd,Rdn,Twy,Apn,Tly,Msc,Fqy,Ser,Uni,Prc,Org,Obs,Ogr,Ppa,Plp,Dpn,Vor,Dme,Ndb,Ahu,Fto,Tlo,Tls,Rls,Fdn,Fdd,Fdo,Fda,Fls,Tla,Sae,Aha"


        Dim cf As New coreFeatureStruct

        If filter.Contains("Ase") Then
            cf.FeatureName = "Ase"
            cf.relatedFeatureNames = {cf.FeatureName, "Abd", "Adg", "Sae"}
            coreFeatures.Add(cf)
        End If
        If filter.Contains("Uni") Then
            cf.FeatureName = "Uni"
            cf.relatedFeatureNames = {cf.FeatureName, "Ser", "Fqy"}
            coreFeatures.Add(cf)
        End If
        If filter.Contains("Ahp") Then
            cf.FeatureName = "Ahp"
            cf.relatedFeatureNames = {cf.FeatureName, "Rwy", "Rdn", "Rdd", "Ful", "Aga", "Twy", "Apn", "Tly", "Msc", "Ahu", "Aha"}
            coreFeatures.Add(cf)
        End If
        If filter.Contains("Vor") Then
            cf.FeatureName = "Vor"
            cf.relatedFeatureNames = {cf.FeatureName, "Dme"}
            coreFeatures.Add(cf)
        End If
        If filter.Contains("Dme") Then
            cf.FeatureName = "Dme"
            cf.relatedFeatureNames = {cf.FeatureName, "Dme"}
            coreFeatures.Add(cf)
        End If
        If filter.Contains("Dpn") Then
            cf.FeatureName = "Dpn"
            cf.relatedFeatureNames = {cf.FeatureName}
            coreFeatures.Add(cf)
        End If
        If filter.Contains("Ndb") Then
            cf.FeatureName = "Ndb"
            cf.relatedFeatureNames = {cf.FeatureName}
            coreFeatures.Add(cf)
        End If
        If filter.Contains("Vor") Then
            cf.FeatureName = "Vor"
            cf.relatedFeatureNames = {cf.FeatureName, "Tcn"}
            coreFeatures.Add(cf)
        End If
        If filter.Contains("Ogr") Then
            cf.FeatureName = "Ogr"
            cf.relatedFeatureNames = {cf.FeatureName, "Obs"}
            coreFeatures.Add(cf)
        End If






        ' make one array with all allowed features
        Dim mkArr As New List(Of String)
        For Each cf In coreFeatures
            For Each m In cf.relatedFeatureNames
                mkArr.Add(m)
            Next
        Next

        ' ++++++++++++++++++++++++++++++++
        ' Attach UIDs
        ' ++++++++++++++++++++++++++++++++

        If directory <> "" Then

            ' console only give uids 
            Console.WriteLine("calculate ids in the following directory: " & directory)
            Dim di As New DirectoryInfo(directory)
            ' Get a reference to each file in that directory.
            Dim fiArr As FileInfo() = di.GetFiles()
            ' Display the names of the files.
            Dim fri As FileInfo
            For Each fri In fiArr
                If fri.Name.EndsWith(".ofmx") Then
                    Dim doc As New Xml.XmlDocument()
                    doc.Load(fri.FullName)

                    Dim outdir = Path.GetDirectoryName(fri.FullName) & "\ofmx.uuid\" & fri.Name
                    attachUUIDs(doc, outdir.Replace(".ofmx", ".uuid.ofmx"), coreFeatures, mkArr.ToArray())
                End If

            Next fri

        Else
            Dim masterIds = attachUUIDs(master, master.BaseURI.Replace(".ofmx", ".uuid.ofmx").Replace("file:///", ""), coreFeatures, mkArr.ToArray())
            If slave IsNot Nothing Then
                If slave.BaseURI <> master.BaseURI Then
                    Dim slaveIds = attachUUIDs(slave, slave.BaseURI.Replace(".ofmx", ".uuid.ofmx").Replace("file:///", ""), coreFeatures, mkArr.ToArray())

                    writeChangesFile(masterIds, slaveIds)
                End If
            End If
        End If




        Console.WriteLine("task finished")

    End Sub

End Module
