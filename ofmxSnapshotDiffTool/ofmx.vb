﻿Imports System.IO
Imports System.Security.Cryptography
Imports System.Text
Imports System.Xml
Imports Microsoft.VisualBasic

Module ofmx

    Structure contentHashStruct
        Dim contentHash As String
        Dim coreFeatureMainNodeHash As String
        Dim connectedMids As List(Of String)
        Dim allowedFeatures As List(Of String)
        Dim _content As Object
        Dim status_1Add_2Update_3Delete As Short

    End Structure
    Structure coreFeatureStruct
        Dim FeatureName As String
        Dim relatedFeatureNames() As String
    End Structure


    Dim lastCoreFeature As Long
    Function attachUUIDs(document As Xml.XmlDocument, filename As String, coreFeatures As List(Of coreFeatureStruct), allowdFeatures() As String) As List(Of contentHashStruct)

        lastCoreFeature = 0

        ' childnode #1 is the ofmx-snapshot node
        Dim orig_attr As New List(Of XmlAttribute)

        For Each item In document.ChildNodes(1).Attributes
            orig_attr.Add(item)
        Next


        Dim hotMids As New List(Of contentHashStruct) ' those mids are triggered To be exchanged If content hash (topIds) have triggered a change

        dbDict.Clear()

        ReDim doubleCheck_ParentNode_Dict(0)



        Dim node As Xml.XmlNode = document("OFMX-Snapshot")

        If node IsNot Nothing Then


            Dim topIds(node.ChildNodes.Count - 1) As String
            Dim region = CLASS_region
            Try
                region = node.Attributes("regions").Value
            Catch ex As Exception

            End Try




            Dim allComments As Xml.XmlNodeList = document.SelectNodes("//comment()")
            For Each n As Xml.XmlNode In allComments
                n.ParentNode.RemoveChild(n)
            Next


            Dim cpos As Long = Console.CursorTop
            Dim currPerc As Long = 0
            Dim lst(0) As String
            Dim coreFeatureHash As String = ""
            Dim _cont As Object = Nothing

            cpos = Console.CursorTop

            Dim lastTs As Date = Date.Now


            Dim nodeStringList As New List(Of Xml.XmlNode)
            For top As Long = 0 To node.ChildNodes.Count - 1


                Dim nodeClone As Xml.XmlNode = node.ChildNodes(top).Clone



                'Console.SetCursorPosition(0, cpos)

                If Date.Now.Subtract(lastTs).Ticks > TimeSpan.TicksPerMillisecond * 5000 Then
                    Dim perc As Double = Math.Round(top / (node.ChildNodes.Count - 1) * 100, 0)
                    lastTs = Date.Now
                    For i As Short = 0 To 50

                        If i = 0 Or i = 50 Then
                            Console.Write("|")
                        End If

                        If i < perc / 2 Then
                            Console.Write("=")
                        Else
                            Console.Write(" ")
                        End If

                        currPerc = perc
                    Next
                    Console.WriteLine(" processing, please wait.. " & top & " nodes read.. progress:" & perc)

                End If







                For f As Short = 0 To node.ChildNodes(top).ChildNodes.Count - 1



                    If allowdFeatures.Contains(nodeClone.Name) Then


                        attachId(nodeClone.ChildNodes(f), nodeClone, region)
                        If nodeClone.ChildNodes(f) IsNot Nothing Then


                            If nodeClone.ChildNodes(f).HasChildNodes Then


                                For Each q In nodeClone.ChildNodes(f)
                                    attachId(q, nodeClone.ChildNodes(f), region)

                                    If q.ChildNodes IsNot Nothing Then
                                        If q.ChildNodes.Count > 0 Then
                                            For Each r In q.ChildNodes
                                                attachId(r, q, region)

                                                If r.ChildNodes IsNot Nothing Then
                                                    If r.ChildNodes.Count > 0 Then
                                                        For Each fsf In r.ChildNodes
                                                            attachId(fsf, r, region)

                                                            If fsf.ChildNodes IsNot Nothing Then
                                                                If fsf.ChildNodes.Count > 0 Then
                                                                    For Each faf In q.ChildNodes
                                                                        attachId(faf, fsf, region)
                                                                    Next
                                                                End If
                                                            End If
                                                        Next
                                                    End If
                                                End If

                                            Next
                                        End If
                                    End If
                                Next
                            End If
                        End If
                    Else
                        nodeClone.RemoveAll()
                    End If


                Next



                ' get all sub mid of xxxUid nodes

                ' find allowed related features




                'node.ChildNodes(top)


                If node.ChildNodes(0).ChildNodes(0) IsNot Nothing Then


                    For Each item In coreFeatures


                        ' special case first hash
                        If top = 0 Then
                            Try
                                If node.ChildNodes(0).ChildNodes(0).Attributes("mid") IsNot Nothing Then coreFeatureHash = node.ChildNodes(0).ChildNodes(0).Attributes("mid").Value
                                _cont = node.ChildNodes(0)
                            Catch ex As Exception

                            End Try



                        End If

                        If nodeClone.Name.StartsWith(item.FeatureName) Then



                            Dim cs As New contentHashStruct
                            cs.contentHash = topIds(lastCoreFeature)

                            If topIds(lastCoreFeature) IsNot Nothing Then
                                cs.connectedMids = New List(Of String)
                                cs.coreFeatureMainNodeHash = coreFeatureHash
                                cs._content = _cont



                                ' add element of last core feature
                                For Each m In neigbourMids
                                    If cs.connectedMids.Contains(m.mid) = False Then
                                        cs.connectedMids.Add(m.mid)
                                        cs.allowedFeatures = m.allowdTags.ToList
                                    End If


                                Next

                                neigbourMids.Clear()
                                hotMids.Add(cs)


                                _cont = nodeClone
                                If nodeClone.ChildNodes.Count > 0 Then
                                    coreFeatureHash = nodeClone.ChildNodes(0).Attributes("mid").Value
                                End If

                            End If
                            lst = item.relatedFeatureNames
                            lastCoreFeature = top
                        End If
                    Next


                    listId(nodeClone, lst)
                    Dim contentHash = getUUID(nodeClone.InnerText)
                    topIds(lastCoreFeature) &= contentHash



                    ' dont forget the last one:
                    If node.ChildNodes.Count - 1 = top Then




                        Dim cs_last As New contentHashStruct
                        cs_last.contentHash = topIds(lastCoreFeature)

                        If node.ChildNodes(lastCoreFeature).ChildNodes.Count > 0 Then
                            If nodeClone.ChildNodes(0) IsNot Nothing Then coreFeatureHash = nodeClone.ChildNodes(0).Attributes("mid").Value
                        End If

                        If topIds(lastCoreFeature) IsNot Nothing Then
                            cs_last.connectedMids = New List(Of String)



                            cs_last.coreFeatureMainNodeHash = coreFeatureHash
                            cs_last._content = _cont

                            ' add element of last core feature
                            For Each m In neigbourMids
                                If cs_last.connectedMids.Contains(m.mid) = False Then
                                    cs_last.connectedMids.Add(m.mid)
                                    cs_last.allowedFeatures = m.allowdTags.ToList
                                End If
                            Next

                            neigbourMids.Clear()
                            hotMids.Add(cs_last)
                        End If
                    End If
                End If

                nodeStringList.Add(nodeClone)
            Next
            node.RemoveAll()

            For Each nodeX In nodeStringList
                node.AppendChild(nodeX)

            Next

        End If





        ' remove empty nodes

        For i As Long = 0 To document.DocumentElement.ChildNodes.Count - 1

            If document.DocumentElement.ChildNodes(i) IsNot Nothing Then
                If document.DocumentElement.ChildNodes(i).HasChildNodes = False Then
                    document.DocumentElement.ChildNodes(i).ParentNode.RemoveChild(document.DocumentElement.ChildNodes(i))
                End If
            End If

        Next


        For Each tt In orig_attr
            If Not document("OFMX-Snapshot").HasAttribute(tt.Name) Then document("OFMX-Snapshot").SetAttribute(tt.Name, tt.Value)
        Next

        ' set attributes
        If Not document("OFMX-Snapshot").HasAttribute("region") Then document("OFMX-Snapshot").SetAttribute("region", CLASS_region)
        If CLASS_bounds <> "" Then If Not document("OFMX-Snapshot").HasAttribute("boundingbox") Then document("OFMX-Snapshot").SetAttribute("boundingbox", CLASS_bounds)


        Dim xmlRep As String = document.OuterXml

        Dim doc = XDocument.Parse(document.OuterXml)





        Dim emptyElements = From descendant In doc.Descendants() Where descendant.IsEmpty OrElse String.IsNullOrWhiteSpace(descendant.Value) Select descendant
        emptyElements.Remove()

        Dim outDir As String = Path.GetDirectoryName(filename)
        If Not System.IO.Directory.Exists(outDir) Then
            System.IO.Directory.CreateDirectory(outDir)
        End If

        If filename <> "" Then doc.Save(filename)


        Return hotMids

    End Function

    Dim neigbourMids As New List(Of neigbourMidsStruct)


    Structure neigbourMidsStruct
        Dim mid As String
        Dim allowdTags() As String
    End Structure


    Sub listId(node As Xml.XmlNode, lst() As String)

        If node.Name.Length = 3 Then
            ' core featurei
            If node(node.Name & "Uid") IsNot Nothing Then
                If node(node.Name & "Uid").Attributes("mid") IsNot Nothing Then

                    Dim cc As neigbourMidsStruct
                    cc.mid = node(node.Name & "Uid").Attributes("mid").Value
                    cc.allowdTags = lst

                    neigbourMids.Add(cc)

                End If

            End If

        End If

        If node.Attributes IsNot Nothing Then
            If node.Attributes("mid") IsNot Nothing Then
                If filter.Contains(node.Name.Substring(0, 3)) Then


                    Dim cc As neigbourMidsStruct
                    cc.mid = node.Attributes("mid").Value
                    cc.allowdTags = lst

                    neigbourMids.Add(cc)


                End If
            End If
        End If

        For Each nodeLevel5 As Xml.XmlNode In node
            listId(nodeLevel5, lst)
        Next
    End Sub


    ' id work
    Dim dbDict As New Dictionary(Of String, Long)
    Dim dbCntr As Long = 0

    Dim doubleCheck_ParentNode_Dict() As List(Of String)



    Sub attachId(node As Xml.XmlNode, topNode As Xml.XmlNode, region As String)

        If node IsNot Nothing Then
            If node.FirstChild IsNot Nothing Then



                If node.Name.StartsWith("Adg") Or node.Name.EndsWith("Uid") Then


                    Dim regHash = ""
                    Dim md5 As MD5 = System.Security.Cryptography.MD5.Create()
                    Dim inputBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(region)
                    Dim hash As Byte() = md5.ComputeHash(inputBytes)
                    Dim sb As New StringBuilder()
                    For i As Integer = 0 To hash.Length - 1
                        sb.Append(hash(i).ToString("x2"))
                    Next
                    regHash = (sb.ToString.Replace("a", "").Replace("b", "").Replace("c", "").Replace("d", "").Replace("e", "").Replace("f", "")).Substring(0, 6)

                    If node Is Nothing Then
                        Return
                    End If

                    If topNode(node.Name) Is Nothing Then Return

                    ' handle AseUidSameExtent
                    If node.Name.Contains("Adg") Then
                        Dim parent = node.ParentNode
                        Dim SEnode = parent("AseUidSameExtent")
                        Dim uidStr = region & "|AseUid|"

                        For Each it In SEnode.ChildNodes
                            uidStr &= it.innerText & "|"
                        Next



                        ' find the correct id:


                        uidStr = uidStr.TrimEnd("|")
                        Dim uuid = getUUID(uidStr)

                        For Each item In dbDict
                            If item.Key = uuid Then

                                SEnode.SetAttribute("dbUid", regHash & item.Value)
                                Exit For

                            End If
                        Next



                    End If

                    If node.Name.EndsWith("Uid") Then
                        Dim uidStr = ""
                        For Each it In node
                            uidStr &= it.innerText & "|"
                        Next

                        uidStr = uidStr.TrimEnd("|")
                        Dim uuid = getUUID(region & "|" & node.Name & "|" & uidStr)


                        Dim dbId = dbCntr
                        If dbDict.ContainsKey(uuid) Then
                            dbId = dbDict(uuid)





                        Else
                            dbDict.Add(uuid, dbId)
                            ' add header



                            dbCntr += 1
                        End If




                        ' check it has a different header - get rid of duplications
                        ' --------------------------------------->
                        Dim k = node.FirstChild
                        Dim Addr As String = ""
                        If k IsNot Nothing Then
                            Do Until k.ParentNode Is Nothing

                                If k.FirstChild IsNot Nothing Then
                                    If k.FirstChild.Name.EndsWith("Uid") Then
                                        Addr &= k.FirstChild.Name & "#" & k.FirstChild.InnerText & "#|"
                                    End If
                                End If

                                k = k.ParentNode
                            Loop
                        End If



                        If dbId > doubleCheck_ParentNode_Dict.Count - 1 Then
                            ReDim Preserve doubleCheck_ParentNode_Dict(dbId)
                        End If



                        If doubleCheck_ParentNode_Dict(dbId) IsNot Nothing Then
                            If doubleCheck_ParentNode_Dict(dbId).Contains(Addr) Then
                                ' If directory = "" Then Console.WriteLine("duplicated entry: " & uuid & " -> " & Addr)


                            End If
                        End If

                        If doubleCheck_ParentNode_Dict(dbId) Is Nothing Then
                            doubleCheck_ParentNode_Dict(dbId) = New List(Of String)
                        Else
                            Dim kkkk = 3
                        End If
                        doubleCheck_ParentNode_Dict(dbId).Add(Addr)
                        ' --------------------------------------->

                        If topNode(node.Name) IsNot Nothing Then


                            topNode(node.Name).SetAttribute("mid", uuid)




                            topNode(node.Name).SetAttribute("dbUid", regHash & dbId)
                            topNode(node.Name).SetAttribute("region", region)
                            'Console.Write("*")

                        End If
                    End If
                End If
            End If
                End If
    End Sub
    Public Function getUUID(uidStr As String) As String


        If uidStr Is Nothing Then Return ""
        Using hasher As MD5 = MD5.Create()    ' create hash object

            ' Convert to byte array and get hash
            Dim dbytes As Byte() =
             hasher.ComputeHash(Encoding.UTF8.GetBytes(uidStr))

            ' sb to create string from bytes
            Dim sBuilder As New StringBuilder()

            ' convert byte data to hex string
            For n As Integer = 0 To dbytes.Length - 1
                sBuilder.Append(dbytes(n).ToString("X2"))
            Next n

            Dim rawhash As Char() = sBuilder.ToString().ToLower

            Dim finalUuid As String = ""
            Dim cnt As Short = 0
            For Each letter In rawhash

                If cnt = 8 Or cnt = 12 Or cnt = 16 Or cnt = 20 Then
                    finalUuid &= "-"
                End If
                finalUuid &= letter
                cnt += 1
            Next


            Return finalUuid
        End Using


    End Function

    Dim additions As New List(Of String)
    Dim deletions As New List(Of String)
    Sub writeChangesFile(masterF As List(Of contentHashStruct), slaveF As List(Of contentHashStruct))
        ' get additions:

        If slaveF.Count = 0 Then Return


        Dim mmF As New List(Of String)
        For Each item In masterF
            mmF.Add(item.contentHash)
        Next

        Dim mmS As New List(Of String)
        For Each item In slaveF
            mmS.Add(item.contentHash)
        Next

        Dim changes As New List(Of contentHashStruct)

        Dim ignoreForDeletions As New List(Of String)
        For itemCntr As Long = 0 To mmF.Count - 1
            Dim item = mmF(itemCntr)
            If mmS.Contains(item) = False Then
                Dim rr = masterF(itemCntr)
                rr.status_1Add_2Update_3Delete = 2 ' by default
                masterF(itemCntr) = rr

                changes.Add(masterF(itemCntr))
                ignoreForDeletions.Add(rr.coreFeatureMainNodeHash)
                Try
                    Console.WriteLine("change detected: " & masterF(itemCntr)._content.innerText)
                Catch ex As Exception

                End Try

            End If
        Next

        For itemCntr As Long = 0 To mmS.Count - 1
            Dim item = mmS(itemCntr)
            If mmF.Contains(item) = False Then


                Dim rr = slaveF(itemCntr)
                If ignoreForDeletions.Contains(rr.coreFeatureMainNodeHash) = False Then
                    rr.status_1Add_2Update_3Delete = 3 ' by default
                    slaveF(itemCntr) = rr

                    changes.Add(slaveF(itemCntr))
                    Console.WriteLine("core feature commit required: " & slaveF(itemCntr)._content.innerText)
                End If



            End If
        Next



        ' get update statuts
        Dim mmF_mid As New List(Of String)
        For Each item In masterF
            Try
                If item.connectedMids.Count > 0 Then
                    mmF_mid.Add(item.connectedMids(0))  'mid 0 is always the core features mid
                End If
            Catch ex As Exception
            End Try

        Next

        Dim mmS_mid As New List(Of String)
        For Each item In slaveF
            Try
                If item.connectedMids.Count > 0 Then
                    mmS_mid.Add(item.connectedMids(0))
                End If
            Catch ex As Exception
            End Try

        Next


        ' get deletions
        Dim cn As Long = -1
        For Each item In mmS_mid
            cn += 1
            If mmF_mid.Contains(item) = False Then
                Dim kk = masterF(cn)
                kk.status_1Add_2Update_3Delete = 3
                masterF(cn) = kk
                Console.WriteLine("deletion detected: " & item)
            End If
        Next


        Dim changesDoc As Xml.XmlDocument = master.Clone

        ' delete all nodes except of first
        changesDoc.DocumentElement.RemoveAll()
        For Each att In master.DocumentElement.Attributes
            changesDoc.DocumentElement.SetAttribute(att.name, att.value)
        Next

        ' +++++ master first  -> which means addition +++++

        Dim added As New List(Of String)

        ' build dict
        Dim idxMidDict_master As New Dictionary(Of String, Long)
        Dim idxMidDict_slave As New Dictionary(Of String, Long)
        For mItemCnt2 As Long = 0 To master.ChildNodes(1).ChildNodes.Count - 1
            Try
                If master.ChildNodes(1).ChildNodes(mItemCnt2).ChildNodes.Count > 0 Then idxMidDict_master.Add(master.ChildNodes(1).ChildNodes(mItemCnt2).ChildNodes(0).Attributes("mid").Value, mItemCnt2)
            Catch ex As Exception

            End Try
        Next
        For mItemCnt1 As Long = 0 To slave.ChildNodes(1).ChildNodes.Count - 1
            Try
                If slave.ChildNodes(1).ChildNodes(mItemCnt1).ChildNodes.Count > 0 Then idxMidDict_slave.Add(slave.ChildNodes(1).ChildNodes(mItemCnt1).ChildNodes(0).Attributes("mid").Value, mItemCnt1)
            Catch ex As Exception

            End Try
        Next

        For Each el In changes
            Try
                Console.WriteLine(" assemble new change: " & el._content.innertext)
            Catch ex As Exception

            End Try


            ' masterfile (for additions and updates)
            'For mItemCnt As Long = 0 To master.ChildNodes(1).ChildNodes.Count - 1

            If idxMidDict_master.ContainsKey(el.coreFeatureMainNodeHash) Then



                Dim mItem = master.ChildNodes(1).ChildNodes(idxMidDict_master(el.coreFeatureMainNodeHash))

                ' find the core feature first node
                If mItem.ChildNodes(0) IsNot Nothing Then

                    If filter.Contains(mItem.Name) Then
                        If mItem.ChildNodes(0).Attributes("mid") IsNot Nothing Then
                            If mItem.ChildNodes(0).Attributes("mid").Value = el.coreFeatureMainNodeHash Then


                                Dim mItemCnt = idxMidDict_master(el.coreFeatureMainNodeHash)
                                Dim cntUpper As Long = mItemCnt + 100

                                If cntUpper >= master.ChildNodes(1).ChildNodes.Count - 1 Then
                                    cntUpper = master.ChildNodes(1).ChildNodes.Count - 1
                                End If

                                ' core feature identified, add this corefeature plus the following ids, until the next corefeature
                                For findIdsM As Long = mItemCnt To cntUpper

                                    Dim elf = master.ChildNodes(1).ChildNodes(findIdsM)
                                    If el.allowedFeatures.Contains(elf.Name) And elf.InnerXml.Contains(el.coreFeatureMainNodeHash) Then ' make sure feature is approved
                                        Dim mid As String = ""



                                        For Each mid In el.connectedMids

                                            If elf.ChildNodes(0) IsNot Nothing Then

                                                Dim ff = False
                                                For Each item In el.connectedMids
                                                    If elf.InnerXml.Contains(item) Then
                                                        ff = True

                                                        'el.connectedMids.Remove(item)
                                                        Exit For
                                                    End If

                                                Next


                                                If ff Then


                                                    Console.Write(" + . " & elf.Name & " .")
                                                    Dim tempNode = changesDoc.ImportNode(elf, True)
                                                    Dim newAttr As Xml.XmlAttribute = changesDoc.CreateAttribute("m")
                                                    newAttr.Value = "+"
                                                    tempNode.FirstChild.Attributes.Append(newAttr)

                                                    ' set isNewEntity to add all elements
                                                    Dim newEntityAttr As Xml.XmlAttribute = changesDoc.CreateAttribute("newEntity")
                                                    newEntityAttr.Value = True
                                                    tempNode.FirstChild.Attributes.Append(newEntityAttr)
                                                    Dim duplicateHash = getUUID(tempNode.InnerXml)
                                                    If Not added.Contains(duplicateHash) Then
                                                        changesDoc.DocumentElement.AppendChild(tempNode)
                                                        added.Add(duplicateHash)
                                                        Exit For
                                                    End If

                                                    mItemCnt = findIdsM ' to avoid double processing
                                                    Exit For
                                                End If
                                            End If


                                        Next
                                    End If
                                Next
                            End If
                        End If
                    End If
                End If
                'Next
            End If



            ' slave files, for deletions
            If el.status_1Add_2Update_3Delete = 3 Then
                If idxMidDict_slave.ContainsKey(el.coreFeatureMainNodeHash) Then


                    Dim mItem = slave.ChildNodes(1).ChildNodes(idxMidDict_slave(el.coreFeatureMainNodeHash))

                    Dim mItemCnt = idxMidDict_slave(el.coreFeatureMainNodeHash)

                    ' find the core feature first node

                    If mItem.ChildNodes(0) IsNot Nothing Then
                        If mItem.ChildNodes(0).Attributes("mid") IsNot Nothing Then
                            If mItem.ChildNodes(0).Attributes("mid").Value = el.coreFeatureMainNodeHash Then
                                If filter.Contains(mItem.Name) Then



                                    Dim cntUpper As Long = mItemCnt + 100

                                    If cntUpper > slave.ChildNodes(1).ChildNodes.Count - 1 Then cntUpper = slave.ChildNodes(1).ChildNodes.Count - 1

                                    ' core feature identified, add this corefeature plus the following ids, until the next corefeature
                                    For findIdsM As Long = mItemCnt To cntUpper

                                        Dim elf = slave.ChildNodes(1).ChildNodes(findIdsM)
                                        If el.allowedFeatures.Contains(elf.Name) Then ' make sure feature is approved
                                            Dim mid As String = ""
                                            For Each mid In el.connectedMids
                                                If elf.InnerXml.Contains(mid) And elf.InnerXml.Contains(el.connectedMids(0)) Then
                                                    Dim k = elf.InnerXml

                                                    Console.Write(" + . " & elf.Name & " .")
                                                    Dim tempNode = changesDoc.ImportNode(elf, True)
                                                    Dim newAttr As Xml.XmlAttribute = changesDoc.CreateAttribute("m")

                                                    If el.status_1Add_2Update_3Delete = 3 Then
                                                        newAttr.Value = "-"
                                                    Else
                                                        newAttr.Value = "+"
                                                    End If

                                                    tempNode.FirstChild.Attributes.Append(newAttr)

                                                    ' set isNewEntity to add all elements
                                                    Dim newEntityAttr As Xml.XmlAttribute = changesDoc.CreateAttribute("newEntity")
                                                    newEntityAttr.Value = True
                                                    tempNode.FirstChild.Attributes.Append(newEntityAttr)
                                                    Dim duplicateHash = getUUID(tempNode.InnerXml)
                                                    If Not added.Contains(duplicateHash) Then
                                                        changesDoc.DocumentElement.AppendChild(tempNode)
                                                        added.Add(duplicateHash)
                                                        Exit For
                                                    End If

                                                    mItemCnt = findIdsM ' to avoid double processing
                                                    Exit For
                                                End If
                                            Next
                                        End If
                                    Next
                                End If
                            End If
                        End If
                    End If
                End If

            End If
        Next

        changesDoc.Save("diff.ofmx")

    End Sub
End Module
